# dobot_sub

Dobot MagicianをROS2で制御する。

## Environment

- Ubuntu 22.04
- ROS2 Humble

## Directory Structure
Dobot Magicianとの通信のため[外部のAPI](https://github.com/AlexGustafsson/dobot-python)を利用している。  
当該APIは本リポジトリで管理していないので、本リポジトリのプログラムを実行する場合は以下の通りにディレクトリ構成を整える必要がある。  

![ディレクトリ構成](assets/ディレクトリ構成.png)

dobot_sub(本リポジトリ)と同じ階層にdobot_interfaceがあり、dobot_interfaceの中にdobot_python(外部のAPI)がある形。  

また、.bashrcには以下を追記している。

```
# anyenv
#export PATH=~/.anyenv/bin:$PATH
#eval "$(anyenv init -)"

# ros
source /opt/ros/humble/setup.bash

# python_orocos_kdl
# これはいらないかも
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

# python path
# 外部APIをdobot_subからimportするために必要
export PYTHONPATH="$(cd; echo $(pwd))/projects:$PYTHONPATH"
```
