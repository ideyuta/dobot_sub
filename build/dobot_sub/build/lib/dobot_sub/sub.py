import time
import rclpy
from rclpy.node import Node
from std_msgs.msg import Float32MultiArray

# from dobot_interface.tcp_ip_4axis_python.dobot_api import DobotApiDashboard, DobotApi, DobotApiMove
from dobot_interface.dobot_python.interface import Interface as DobotInterface


class MinimalSubscriber(Node):
    def __init__(self, dobot_controller):
        super().__init__("minimal_subscriber")
        self.subscription = self.create_subscription(
            Float32MultiArray,
            "dobot_control",
            self.listener_callback,
            10)
        self.subscription  # prevent unused variable warning
        
        # 現在姿勢を定期的に確認するために追加
        timer_period = 2
        self.timer = self.create_timer(timer_period, self.timer_callback)

        self.controller = dobot_controller
        
    def listener_callback(self, msg):
        x = msg.data[0]
        y = msg.data[1]
        z = msg.data[2]
        r = msg.data[3]
        print(f"x:{x}, y:{y}, z:{z}, r:{r}")
        # アームの動作指示
        # self.controller.move_to(x, y, z, r)

    def timer_callback(self):
        # 現在姿勢を確認
        print(f"current pose: {self.controller.get_pose()}")


class DobotController(DobotInterface):
    def __init__(self, port):
        self.port = port
        self.controller = DobotInterface(self.port) # connect

    # Wait until the instruction finishes
    def wait(self, queue_index=None):
        # If there are no more instructions in the queue, it will end up
        # always returning the last instruction - even if it has finished.
        # Use a zero wait as a non-operation to bypass this limitation
        self.controller.wait(0)
        if queue_index is None:
            queue_index = self.controller.get_current_queue_index()
        while True:
            if self.controller.get_current_queue_index() > queue_index:
                break
            time.sleep(0.5)
    
    def get_pose(self):
        return self.controller.get_pose()

    # Homing
    def home(self, wait=True):
        self.controller.set_homing_command(0)
        if wait:
            self.wait()
    
    # Move to the absolute coordinate, one axis at a time
    def move_to(self, x, y, z, r, wait=True):
        self.controller.set_point_to_point_command(3, x, y, z, r)
        if wait:
            self.wait()
    
    def set_end_effector_gripper(self, enable_control, enable_grip):
        self.controller.set_end_effector_gripper(enable_control, enable_grip)


class DobotWrapper:
    def __init__(self, port):
        self.controller = DobotController(port)
        self.minimal_subscriber = MinimalSubscriber(self.controller)

    def run(self):
        print("Exec homing")
        self.controller.home()
        print("Done homing")

        # グリッパーの開閉、吸引のON/OFF(いずれも空気制御なのでコードは同じでOK)
        self.controller.set_end_effector_gripper(True, False)
        self.controller.set_end_effector_gripper(True, True)
        time.sleep(5)
        self.controller.set_end_effector_gripper(True, False)        
        self.controller.set_end_effector_gripper(False, False)

        print("Exec rclpy.spin")
        rclpy.spin(self.minimal_subscriber)


def main(args=None):
    rclpy.init(args=args)
    dobot_wrapper = DobotWrapper("/dev/ttyACM0")
    dobot_wrapper.run()

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    minimal_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
